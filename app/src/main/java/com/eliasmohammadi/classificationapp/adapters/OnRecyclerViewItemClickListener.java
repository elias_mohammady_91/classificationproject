package com.eliasmohammadi.classificationapp.adapters;

public interface OnRecyclerViewItemClickListener {

    void onClick(String name);

}

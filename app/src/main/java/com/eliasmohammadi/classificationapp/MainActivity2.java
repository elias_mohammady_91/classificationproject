package com.eliasmohammadi.classificationapp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.widget.Toast;

import com.eliasmohammadi.classificationapp.adapters.ItemRecyclerViewAdapter;
import com.eliasmohammadi.classificationapp.adapters.OnRecyclerViewItemClickListener;

import java.util.ArrayList;
import java.util.List;

public class MainActivity2 extends AppCompatActivity {
private RecyclerView rc;
private ItemRecyclerViewAdapter adapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        rc = findViewById(R.id.rc);
        rc.setLayoutManager(new LinearLayoutManager(this,RecyclerView.VERTICAL,false));

        List<String> names = new ArrayList<>();
        names.add("elias");
        names.add("amirhossein");
        names.add("erfan");
        names.add("ehsan");

        adapter = new ItemRecyclerViewAdapter(names, new OnRecyclerViewItemClickListener() {
            @Override
            public void onClick(String name) {
                Toast.makeText(MainActivity2.this,name,Toast.LENGTH_LONG).show();
            }
        });

        rc.setAdapter(adapter);



    }
}
package com.eliasmohammadi.classificationapp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.eliasmohammadi.classificationapp.view.LoginFragment;

public class MainActivity extends AppCompatActivity implements ICallable {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        loadFragment(new LoginFragment());

    }


    private void loadFragment(Fragment fragment){
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.main_container,fragment)
                .commit();
    }


    @Override
    public void call() {
        Toast.makeText(this,"call From Activity",Toast.LENGTH_LONG).show();
    }
}
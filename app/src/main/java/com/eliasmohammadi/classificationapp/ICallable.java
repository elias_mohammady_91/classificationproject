package com.eliasmohammadi.classificationapp;

public interface ICallable {

    void call();
}

package com.eliasmohammadi.classificationapp.view;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.eliasmohammadi.classificationapp.ICallable;
import com.eliasmohammadi.classificationapp.MainActivity2;
import com.eliasmohammadi.classificationapp.R;

public class LoginFragment extends Fragment {

    private Button btnGoToSignUp;
    private Button btnGotoSecondActivity;
    private ICallable callable;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_login, container, false);

        btnGoToSignUp = view.findViewById(R.id.btn_signup);
        btnGotoSecondActivity = view.findViewById(R.id.btn_goto_second_activity);
        btnGoToSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                getActivity().getSupportFragmentManager().beginTransaction()
                        .replace(R.id.main_container, new SignUpFragment())
                        .commit();

                callable.call();
            }
        });



        btnGotoSecondActivity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getContext(), MainActivity2.class);
                startActivity(i);
                getActivity().finish();
            }
        });


        return view;

    }


    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);

        if(context instanceof ICallable){
            callable = (ICallable) context;
        }


    }
}
